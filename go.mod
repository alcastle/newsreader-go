module mod

go 1.16

require (
	github.com/darkoatanasovski/htmltags v1.0.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/mmcdole/gofeed v1.1.3
	github.com/ogier/pflag v0.0.1
	github.com/pkg/browser v0.0.0-20210706143420-7d21f8c997e2
	github.com/spf13/viper v1.8.1
)
