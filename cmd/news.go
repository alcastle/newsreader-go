package main

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	flag "github.com/ogier/pflag"

	"github.com/mmcdole/gofeed"
	"github.com/darkoatanasovski/htmltags"
	"github.com/spf13/viper"

	"log"
	"math/rand"
	netUrl "net/url"
	"os"
	"os/exec"
	"regexp"
	"runtime"
	"strings"
	"time"
	
	// This is used for future google parsing
	// "github.com/PuerkitoBio/goquery"

	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/browser"
)

var (
	// Command Line Argument Variables
	topic string
	url   string
	voice string

	db     *sql.DB
	err    error
	config Config
	cmd    *exec.Cmd
)

type Config struct {
	dbDriver, dbHost, dbPort, dbName, dbUser, dbPass string
	dbPath, defaultFeed                              string
	speakCommand                                     string
	pause, pauseBrief, pauseLong                     string
	defaultFeeds, confirmationPhrases		       []string
	processInRealTime, enableLearning			     bool
}

// WHY? WOULD YOU FORCE ME TO DO THIS NONSENSE
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

type Feed struct {
	id        int64
	url       string
	createdAt time.Time
	updatedAt time.Time
}

type Headline struct {
	id      int64
	url     string //= item.Link
	title   string //= item.Title
	summary string //= item.Description
	topic   string //= categories(array)

	// other db fields to populate
	feedId    int64 //
	createdAt time.Time
	readAt    time.Time
	isRead    bool
	interest  int64
}

func initFeed() *gofeed.Feed {
	// https://github.com/mmcdole/gofeed/issues/83

	fp := gofeed.NewParser()
	feedData, _ := fp.ParseURL(url)

	log.Println("Fetching: " + url)

	return feedData
}

func createFeed(url string, createdAt time.Time) Feed {

	// Feed doesn't exist so lets add it
	stmt, err := db.Prepare("INSERT INTO feed(url, created_at) VALUES(?, ?)")

	if err != nil {
		log.Fatal(err)
	}

	res, err := stmt.Exec(url, createdAt)

	if err != nil {
		log.Fatal(err)
	}

	lastId, err := res.LastInsertId()

	if err != nil {
		log.Fatal(err)
	}

	// Build the feed struct and return it
	f := Feed{}
	f.id = lastId
	f.url = url
	f.createdAt = createdAt

	return f
}

/**
	Updates the feed table, updated_at column
	@param Feed struct
	@return Feed struct
*/
func updateFeed(feed Feed) Feed {

	res, err := db.Exec("UPDATE feed SET updated_at = ? WHERE id = ?", feed.updatedAt, feed.id)

	if err != nil {
		log.Fatal(err)
	}

	count, err := res.RowsAffected()

	if err != nil || count == 0 {
		read(err.Error())
		log.Fatal(err)
	}

	return feed
}

/**
	Creates a headline row
	@param headline Headline struct
	@return Headline struct
*/
func createHeadline(headline Headline) Headline {

	// Headline doesn't exist so lets add it
	stmt, err := db.Prepare(
		"INSERT INTO headline(url, title, summary, topic, feed_id, created_at, is_read) " +
			"VALUES(?, ?, ?, ?, ?, ?, ?)")

	if err != nil {
		log.Fatal(err)
	}

	res, err := stmt.Exec(headline.url, headline.title, headline.summary,
		headline.topic, headline.feedId, headline.createdAt, headline.isRead)

	if err != nil {
		log.Printf("HeadlineID: %v  Url: %v \n\n", headline.id, headline.url)
		log.Fatal(err)
	}

	lastId, err := res.LastInsertId()

	if err != nil {
		log.Fatal(err)
	}

	headline.id = lastId

	return headline
}

/**
	Updates the headline table, read_at and is_read columns
	@param headline Headline struct
	@return Headline struct
*/
func updateHeadline(headline Headline) Headline {

	res, err := db.Exec("UPDATE headline SET read_at = ?, is_read = true WHERE id = ? ", headline.readAt, headline.id)

	if err != nil {
		log.Fatal(err)
	}

	count, err := res.RowsAffected()

	if err != nil || count == 0 {
		read(err.Error())
		log.Fatal(err)
	}

	return headline
}

func setFeed(link string) Feed {
	log.Printf("Looking for url in feed table: %s\n", link)

	// Do a database lookup - find ony by link
	rows, err := db.Query("SELECT id, url, created_at, updated_at FROM feed WHERE url = ?", link)

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	f := Feed{}

	for rows.Next() {
		var nt NullTime

		// Return the cols to struct vars
		err := rows.Scan(&f.id, &f.url, &f.createdAt, &nt)

		if err != nil {
			if !nt.Valid {
				// fmt.Printf("We are here %v \n", nt)
				//f.updatedAt = nt.Time
			}
		}

		// The feed record exists already
		if f.id > 0 {
			log.Println("Feed exists, updating timestamp")
			// update the struct updated_at field
			f.updatedAt = time.Now()

			if config.dbDriver == "sqlite3" {
				// SQLITE3 will lock without this
				rows.Close()
			}

			// update the feed row
			updateFeed(f)

			return f
		}
	}

	rows.Close()

	// No Feed was found, create a new struct
	if f.id == 0 {
		log.Println("No feed exists, creating")

		// create a new feed record in the database
		f = createFeed(link, time.Now())
	}

	return f
}

func setHeadline(link string) Headline {
	/*
		   Some google headlines are longer than 255 chars.
		   The feeds use the url as an ID; this is our unique key
			Didn't want to change data col type away from varchar
	*/
	if len(link) > 255 {
		hasher := md5.New()
		hasher.Write([]byte(link))

		link = hex.EncodeToString(hasher.Sum(nil))
	}

	// Look to see if this headline exists already
	rows, err := db.Query("SELECT id, feed_id, created_at, title, url, topic, summary, is_read "+
		"FROM headline WHERE url = ?", link)

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	h := Headline{}

	for rows.Next() {
		err := rows.Scan(&h.id, &h.feedId, &h.createdAt, &h.title, &h.url, &h.topic, &h.summary, &h.isRead)

		if err != nil {
			log.Printf("Headline exists already. h.id = %v", h.id)

			return h
		}
	}

	rows.Close()

	// Create a new headline struct since no row exists
	if h.id == 0 {
		log.Println("No headline exists, creating a struct")
		h.url = link
		h.createdAt = time.Now()
		h.isRead = false
	}

	return h
}

/////////// ---- //////////
/**
	Get a list of headlines from the database and read them
	@param	void
	@return void
*/
func getHeadlinesMySQL() {

	rows, err := db.Query("SELECT id, feed_id, created_at, title, url, topic, summary, is_read "+
		"FROM headline WHERE is_read = ? "+
		"ORDER BY created_at DESC", false)

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	h := Headline{}

	for rows.Next() {
		err := rows.Scan(&h.id, &h.feedId, &h.createdAt, &h.title, &h.url, &h.topic, &h.summary, &h.isRead)

		if err != nil {
			log.Fatal(err.Error())
		}

		// TTS - read it
		read(h.summary)

		// Set it to read and persist
		h.readAt = time.Now()
		updateHeadline(h)

		if config.enableLearning == true {
			askInterest(h.topic, h.id, h.url)
		}

	}

	rows.Close()

	_ = db.Close()
}

func getHeadlinesSqlite3() {
	// SQLITE3 can't run and chew bubble gum; going to have to do chunky style db queries.
	var unread int
	row := db.QueryRow("SELECT COUNT(*) as count FROM headline WHERE is_read = 0")
	err := row.Scan(&unread)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Headlines unread: %v \n", unread)

	////////// Pull Content We're Interested In First /////////
	for i := 0; i < unread; i++ {
		// Fetch a single headline
		rows, err := db.Query("SELECT h.id, h.feed_id, h.created_at, h.title, h.url, h.topic, "+
								"h.summary, h.is_read, COUNT(t.interest) AS interest "+
								"FROM headline as h "+
								"LEFT JOIN topic AS t ON t.name = h.topic "+
								"WHERE h.read_at IS NULL "+
								"AND h.is_read = ? "+
								"GROUP BY h.topic "+
								"ORDER BY t.interest DESC, h.created_at ASC "+
								"LIMIT 1", 0)
	
		if err != nil {
			log.Fatal(err)
		}

		h := Headline{}

		for rows.Next() {
			err := rows.Scan(&h.id, &h.feedId, &h.createdAt, &h.title, &h.url, &h.topic, &h.summary, &h.isRead, &h.interest)

			if err != nil {
				log.Fatal(err.Error())
			}
		}
		rows.Close()

		// TTS - read it aloud
		read(h.summary)

		// Set it to read and persist
		h.readAt = time.Now()
		updateHeadline(h)

		if config.enableLearning == true {
			log.Println("Enabled learning - asking for interest")
			askInterest(h.topic, h.id, h.url)
		} else {
			log.Printf("enabled: %s", config.enableLearning)
		}
	}

	_ = db.Close()
}

func getHeadlines() {
	if config.dbDriver == "mysql" {
		getHeadlinesMySQL()
	} else if config.dbDriver == "sqlite3" {
		getHeadlinesSqlite3()
	} else {
		log.Fatalf("Unsupported database driver %v", config.dbDriver)
	}
}
/////////// ---- //////////

func addTopicInterest(topic string, id int64) {
	log.Printf("Topic: %s - id: %s", topic, id)

	stmt, err := db.Prepare("INSERT into topic (headline_id, name, interest) values (?, ?, 1)")

	if err != nil {
		log.Fatal(err.Error())
	}

	res, err := stmt.Exec(id, topic)

	if err != nil {
		log.Printf("Unable to insert new interest: %v \n", err)
	}

	lastId, err := res.LastInsertId()

	if lastId < 0 {
		read("There was an error adding your interests")
		log.Printf("Interests were not inserted: %v \n", err)
	}
}

func askInterest(topic string, id int64, url string) {
	rand.Seed(time.Now().UnixNano())
	phrase := rand.Intn(len(config.confirmationPhrases) -1 ) + 1
	read(config.confirmationPhrases[phrase])

	liked := false
	paused := false

	// Read input - Y / N / YO yes open / NO no open
	reply, _ := bufio.NewReader(os.Stdin).ReadBytes('\n')
	lowerReply := strings.ToLower(string(reply))
	if strings.Contains(lowerReply, "y") {
		// indicated interest
		liked = true;
		if strings.Contains(lowerReply, "o") {
			// indicated wanting to see more
			browser.OpenURL(url)
			paused = true
		}
	} else {
		// They are not interested, but still want to see more
		if strings.Contains(lowerReply, "o") {
			browser.OpenURL(url)
			paused = true 
		}
	}

	if liked == true {
		addTopicInterest(topic, id)
	}

	if paused == true {
		// Pause until user hits enter
		read("Press enter to continue when you're ready")
		bufio.NewReader(os.Stdin).ReadBytes('\n')
	}
}

/**
	Loops over the feed parsing, sanitizing, and storing the data
	@param feedData goFeed.Feed
	@return void
*/
func processFeed(feedData *gofeed.Feed) {

	// Get a Feed struct - either new feed or an existing one
	f := setFeed(feedData.Link)

	for _, item := range feedData.Items {
		h := setHeadline(item.Link)

		// If no headline was found, create the struct and row in database
		if h.id == 0 {
			h.feedId = f.id
			h.createdAt = time.Now()
			h.title = item.Title
			h.isRead = false

			// Google Specific Parsing Handled Here
			if topic != "" {

				/*
					// This all works - just commenting it out for future work

					// Read in content HTML in
					doc, err := goquery.NewDocumentFromReader(strings.NewReader(item.Content))

					// Parse the content for the data we want
					doc.Find("li").Each(func(index int, lineItem *goquery.Selection) {
						text := lineItem.Text()

						// Find the associated link for each headline
						anchorLink, ok := lineItem.Find("a").Attr("href")

						if ok {
							// Debug
							log.Printf("Text: %v \n Link: %v \n\n\n", text, anchorLink)


							// TODO fetch and parse some more
							// TechCrunch - look for <div class="article-content">
							// body := fetchURL(anchorLink)
						}
					})
					os.Exit(1)
				*/

				// attribution separator "\u00a0\u00a0" | Blah blah - Forbes
				nodes, err := htmltags.Strip(item.Content, []string{"ol li"}, true)

				if err != nil {
					log.Fatal(err.Error())
				}

				// fmt.Printf("THIS: %#v \n", nodes.Elements.FirstChild.Data)
				for c := nodes.Elements.FirstChild; c != nil; c = c.NextSibling {
					log.Printf("%v \n\n\n", c.Data)
				}

				summary := sanitize(item.Content)
				h.summary = item.Title + config.pause + strings.ReplaceAll(summary, item.Title, "") + config.pauseBrief
				h.topic = strings.ToLower(topic)

			} else {
				// Sanitize the summary string
				summary := sanitize(item.Description)

				// Builds the string to speak
				h.summary = item.Title + config.pause + summary + config.pauseBrief

				if len(item.Categories) > 0 {
					h.topic = strings.ToLower(item.Categories[0])
				}
			}

			// Persist the data
			createHeadline(h)
		}
	}
}

/**
	Removed troublesome characters that the TTS has trouble with
 	@param string
	@return string
*/
func sanitize(text string) string {
	tmpText := strings.ReplaceAll(
		strings.ReplaceAll(
			strings.ReplaceAll(
				strings.ReplaceAll(
					strings.ReplaceAll(text,
						"&nbsp;", ", "),
					"\"", ""),
				"<nobr>", ""),
			"</nobr>", ""),
		"<wbr>", "")

	nodes, err := htmltags.Strip(tmpText, []string{}, true)
	if err != nil {
		read(err.Error())
		os.Exit(1)
	}

	clean := nodes.ToString()

	// Remove URLs from the content
	re := regexp.MustCompile(`%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu`)
	matched := re.FindString(clean)
	return strings.ReplaceAll(clean, matched, "")
}

/**
TTS - reads aloud a string
*/
func read(text string) {
	log.Println(text)
	if runtime.GOOS == "darwin" {
		// OSX say command
		cmd = exec.Command(config.speakCommand, "--voice=", voice, `"`+text+`"`)
	} else if runtime.GOOS == "linux" {
		// mimic
		if strings.Contains(config.speakCommand, "mimic") {
			cmd = exec.Command(config.speakCommand, "-pw", "-t", `"`+text+`"`)
		} else {
			// espeak, festival, other
			cmd = exec.Command(config.speakCommand, `"`+text+`"`)
		}
	} else if runtime.GOOS == "windows" {
		// TODO Find the windows equivalent
		cmd = exec.Command(config.speakCommand, text)
	}

	err := cmd.Run()

	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err.Error())
	}
}

/**
Establish a connection to the database
*/
func dbInit() {
	// Notes: http://mindbowser.com/golang-go-database-sql/
	//
	// MySQL datetime needs a modifier on the open connection:  ?parseTime=true
	// https://stackoverflow.com/questions/29341590/go-parse-time-from-database/29343013#29343013
	//
	viper.Get("dbDriver")
	log.Println("Attempting a connection for " + config.dbDriver)

	// Loading the DB connection conditionally
	if config.dbDriver == "mysql" {

		db, err = sql.Open(
			config.dbDriver,
			config.dbUser+":"+config.dbPass+"@tcp("+config.dbHost+
				":"+config.dbPort+")/"+config.dbName+
				"?charset=utf8&parseTime=true&loc=Local")

	} else if config.dbDriver == "sqlite3" {

		db, err = sql.Open(config.dbDriver, config.dbPath+"/"+config.dbName+
			".db?cache=shared&mode=rwc&journal_mode=WAL")

	} else {
		log.Fatalf("Unsupported database driver: %v", config.dbDriver)
	}

	if err != nil {
		read("I'm unable to connect to the database, please see the logs")
		log.Fatal(err.Error())
	}

	// sql.Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		read("I'm unable to connect to the database, please see the logs")
		log.Fatal(err.Error())
	}
}

/**
	Will connect to the database server, create database and tables
	if they don't already exist.

	@return void
*/
func dbCreateMySQL() {
	db, _ = sql.Open(config.dbDriver,
		config.dbUser + ":" + config.dbPass + "@tcp(" + config.dbHost + ":" + config.dbPort + ")/")

	stmtDb := "CREATE DATABASE IF NOT EXISTS " + config.dbName + ";"
	statement, _ := db.Prepare(stmtDb)
	result, _ := statement.Exec()
	count, _ := result.RowsAffected()

	if count == 0 {
		// Reset connection to use new database
		dbInit()
	}

	stmtFeed :=
		"CREATE TABLE IF NOT EXISTS `feed` ( " +
		"`id` int(11) NOT NULL AUTO_INCREMENT, " +
		"`url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, " +
		"`created_at` datetime NOT NULL, " +
		"`updated_at` datetime DEFAULT NULL, " +
		"PRIMARY KEY (`id`) " +
		") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;"

	statement2, err := db.Prepare(stmtFeed)
	if err != nil {
		log.Println("Query failed: \n" + stmtFeed)
		log.Fatal(err.Error())
	}
	statement2.Exec()


	stmtHeadline := "CREATE TABLE IF NOT EXISTS `headline` ( " +
		"`id` int(11) NOT NULL AUTO_INCREMENT, " +
		"`feed_id` int(11) NOT NULL, " +
		"`created_at` datetime NOT NULL, " +
		"`title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, " +
		"`url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL, " +
		"`summary` longtext COLLATE utf8mb4_unicode_ci NOT NULL, " +
		"`read_at` datetime DEFAULT NULL, " +
		"`is_read` tinyint(1) DEFAULT NULL, " +
		"PRIMARY KEY (`id`), " +
		"UNIQUE KEY `url_idx` (`url`), " +
		"KEY `IDX_E0E861BD51A5BC03` (`feed_id`), " +
		"CONSTRAINT `FK_E0E861BD51A5BC03` FOREIGN KEY (`feed_id`) REFERENCES `feed` (`id`) " +
		") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;"

	statement3, _ := db.Prepare(stmtHeadline)
	statement3.Exec()


	stmtTopic := "CREATE TABLE IF NOT EXISTS `topic` (" +
		"`id` int(11) unsigned NOT NULL AUTO_INCREMENT, " +
		"`headline_id` int(11) NOT NULL, " +
		"`name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL, " +
		"`interest` int(11) NOT NULL, " +
		"PRIMARY KEY (`id`), " +
		"UNIQUE KEY `topic_idx` (`headline_id`,`name`) " +
		") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;"

	statement4, _ := db.Prepare(stmtTopic)
	statement4.Exec()

	db.Close()

	read("Setup complete. Run again without the setup flag.")
	os.Exit(1)
}

func dbCreateSqlite3() {
	dbInit()

	stmtFeed := "CREATE TABLE IF NOT EXISTS `feed` (" +
		"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"`url` VARCHAR(255) NOT NULL UNIQUE, " +
		"`created_at` DATE NOT NULL, " +
		"`updated_at` DATE DEFAULT NULL " +
		");"

	statement2, err := db.Prepare(stmtFeed)

	if err != nil {
		log.Fatal(err.Error())
	}
	statement2.Exec()

	stmtHeadline := "CREATE TABLE IF NOT EXISTS `headline` ( " +
		"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"`feed_id` INTEGER NOT NULL, " +
		"`created_at` DATE NOT NULL, " +
		"`title` VARCHAR(255)  NOT NULL, " +
		"`url` VARCHAR(255)  NOT NULL UNIQUE, " +
		"`summary` TEXT  NOT NULL, " +
		"`read_at` DATE DEFAULT NULL, " +
		"`is_read` INTEGER DEFAULT NULL, " +
		"FOREIGN KEY (`feed_id`) REFERENCES `feed` (`id`) " +
		");"

	statement3, _ := db.Prepare(stmtHeadline)
	statement3.Exec()

	stmtTopic := "CREATE TABLE IF NOT EXISTS `topic` (" +
		"`id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"`headline_id` INTEGER NOT NULL, " +
		"`name` varchar(50) NOT NULL, " +
		"`interest` INTEGER DEFAULT NULL, " +
		"FOREIGN KEY (`headline_id`) REFERENCES `headline` (`id`) " +
		");"

	statement4, _ := db.Prepare(stmtTopic)
	statement4.Exec()

	db.Close()

	read("Setup complete, you can now run as normal")
	os.Exit(1)
}

func dbCreate() {
	if config.dbDriver == "mysql" {
		dbCreateMySQL()
	} else if config.dbDriver == "sqlite3" {
		dbCreateSqlite3()
	} else {
		log.Fatalf("Unsupported database driver %v", config.dbDriver)
	}
}

/**
	Loads configuration settings from config.yaml
	Sets some default values
*/
func configInit() {
	// Name of the configuration file
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {
		log.Fatalf("Fatal error config file: %s \n", err)
	}

	driver := viper.Get("dbDriver")

	if driver == nil {
		driver = "mysql"
	}

	if driver == "mysql" {
		// Set some defaults for MySQL
		viper.SetDefault("dbDriver", "mysql")
		viper.SetDefault("dbHost", "127.0.0.1")
		viper.SetDefault("dbPort", "3306")
		viper.SetDefault("dbName", "newsreader")
		viper.SetDefault("dbUser", "root")
		viper.SetDefault("dbPass", "")

		config.dbDriver = viper.GetString("dbDriver")
		config.dbHost = viper.GetString("dbHost")
		config.dbPort = viper.GetString("dbPort")
		config.dbName = viper.GetString("dbName")
		config.dbUser = viper.GetString("dbUser")
		config.dbPass = viper.GetString("dbPass")

	} else if driver == "sqlite3" {
		viper.SetDefault("dbDriver", "sqlite3")
		viper.SetDefault("dbPath", "newsreader-go")
		viper.SetDefault("dbName", "newsreader")

		config.dbDriver = viper.GetString("dbDriver")
		config.dbPath = viper.GetString("dbPath")
		config.dbName = viper.GetString("dbName")
	}

	// Ability to handle multiple feeds
	defaultFeeds := make([]string, 10)
	defaultFeeds[0] = "http://rss.slashdot.org/Slashdot/slashdotMainatom"
	viper.SetDefault("defaultFeeds", defaultFeeds)
	config.defaultFeeds = viper.GetStringSlice("defaultFeeds")

	// Ability to handle multiple questions
	confirmationPhrases := make([]string, 10)
	confirmationPhrases[0] = "Did you like this one?"
	viper.SetDefault("confirmationPhrases", confirmationPhrases)
	config.confirmationPhrases = viper.GetStringSlice("confirmationPhrases")

	// Default speakCommand - OSX
	viper.SetDefault("speakCommand", "/usr/bin/say")
	config.speakCommand = viper.GetString("speakCommand")

	viper.SetDefault("enableLearning", true)
	config.enableLearning = viper.GetBool("enableLearning")
}

/**
This gets executed before main() by default
*/
func init() {
	// Show line numbers of error
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Handle command line arguments
	customUrl := flag.String("url", "", "ATOM URL to use")
	customTopic := flag.String("topic", "", "Topic to search for news on")
	customVoice := flag.String("voice", "Samantha", "Alternative Voice to use on OSX")
	customSetup := flag.String("setup", "", "Set to `true` to create database schema")

	flag.Parse()

	voiceArg := *customVoice
	topicArg := *customTopic
	urlArg := *customUrl
	setupArg := *customSetup

	// Load configuration variables
	configInit()

	if voiceArg == "" {
		voice = "Samantha"
	} else {
		voice = voiceArg
	}

	// Default Feed is Slashdot - Topic Searches use Google
	if topicArg == "" && urlArg == "" {
		url = config.defaultFeed

	} else if urlArg != "" {
		url = urlArg

	} else if topicArg != "" {
		topic = topicArg
		url = "https://news.google.com/atom/search?q=" + netUrl.QueryEscape(topic) + "&hl=en-US&gl=US&ceid=US:en"
	}

	log.Println("arg[voice] = " + voice)
	log.Println("arg[topic] = " + topic)
	log.Println("arg[url]   = " + url)
	log.Println("Database Driver = " + config.dbDriver)

	// Will create database and tables
	if setupArg == "true" {
		dbCreate()
	}

	// Setup database connection
	dbInit()
}

func main() {

	for _, url = range config.defaultFeeds {
		// Determine some settings and fetch the ATOM feed
		feedData := initFeed()

		// Parse, sanitize and store
		processFeed(feedData)

		if config.processInRealTime == true {
			// Find headlines not read; TTS
			getHeadlines()
		}
	}

	if config.processInRealTime == false {
		getHeadlines()
	}

	read("There is no new news to read")
}
