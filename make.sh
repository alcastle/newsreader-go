#!/bin/bash

##  Linux 
GOOS=linux GOARCH=amd64 go build -o builds/linux/newsreader cmd/news.go
GOOS=linux GOARCH=386 go build -o builds/linux/newsreader-386 cmd/news.go

##  OSX
GOOS=darwin GOARCH=amd64 go build -o builds/osx/newsreader-amd64 cmd/news.go
#GOOS=darwin GOARCH=386 go build -o builds/osx/newsreader-386 cmd/news.go

##  Windows
GOOS=windows GOARCH=amd64 go build -o builds/win/newsreader.exe cmd/news.go 
